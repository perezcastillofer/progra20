import sqlite3
from email_validator import validate_email

conn = sqlite3.connect('Users.db')


# clase encargada de realizar los cambios en nuestro db

class Field:

    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, obj, objtype=None):
        return conn.execute(self.fetch, [obj.key]).fetchone()[0]

    def __set__(self, obj, value):
        conn.execute(self.store, [value, obj.key])
        conn.commit()

# Clase encargada de realizar cambios según le pasemos un número de teléfono válido o no

class valid_phone:

    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, obj, objtype=None):
        return conn.execute(self.fetch, [obj.key]).fetchone()[0]

    def __set__(self, obj, value):
        try:
            if len(str(value)) != 9:
                raise ValueError
            else:
                conn.execute(self.store, [value, obj.key])
                conn.commit()

        except ValueError:
            print('El número introducido no es válido')


# Esta clase es la encargada de validar la edad

class valid_age:

    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, obj, objtype=None):
        return conn.execute(self.fetch, [obj.key]).fetchone()[0]

    def __set__(self, obj, value):
        try:
            if value > 120:
                raise ValueError
            else:
                conn.execute(self.store, [value, obj.key])
                conn.commit()
        except ValueError:
            print('La edad introducida es inválida')


# Esta clase es la encargada de validar los emails

class valid_email:

    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, obj, objtype=None):
        return conn.execute(self.fetch, [obj.key]).fetchone()[0]

    def __set__(self, obj, value):
        try:
            if validate_email(value):
                conn.execute(self.store, [value, obj.key])
                conn.commit()
        except ValueError:
            print('El email no es válido')


# Con esta clase marcaremos las especificaciones de nuestra tabla y a que otras clases hay que hacer referencia
# para su modificación

class User:
    table = 'Users'
    key = 'id'
    name = Field()
    surname = Field()
    email = valid_email()
    phone_1 = valid_phone()
    phone_2 = valid_phone()
    age = valid_age()

    def __init__(self, key):
        self.key = key


fer = User(1)

# COMPROBACIONES

# fer.age = 40

# assert fer.age == '40'

# fer.age = 200

# print(fer.email)

# fer.email = 'f.perez@uf'

# fer.phone_2 = 98657656799

# print(fer.phone_2)
