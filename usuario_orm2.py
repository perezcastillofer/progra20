import sqlite3

#Connect with Users database
conn = sqlite3.connect('Users.db')


c = conn.cursor()
#Create table
c.execute("""CREATE TABLE IF NOT EXISTS Users (
            id int,
            name text,
            surname text,
            email text,
            phone_1 text,
            phone_2 text,
            age text
            )""")

#Insert data
c.execute("INSERT INTO Users VALUES (1,'Fernando','Perez','fernando@gmail.com',847362738,+47382746352,21)")
c.execute("INSERT INTO Users VALUES (2,'Javier','Sobrini','javier@gmail.com',298465748,+6475839212,23)")
c.execute("INSERT INTO Users VALUES (3,'Santiago','Montero','santiago@gmail.com',123456789,+36453789876,20)")

#Save changes
conn.commit()

#Close connection
conn.close()